#include <linux/sched.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kallsyms.h>
#include <linux/timer.h>

#include <linux/types.h>

#include <linux/fs.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <linux/pagemap.h>
#include <linux/slab.h>
#include <linux/mm.h>

// Time between patch checks
#define BT_PROC_PATCH_INTERVAL_MS       5000  // 5 seconds

// Target
#define BTP_TARGET_PROCESS      "droid.bluetooth"
#define BTP_TARGET_SO           "bluetooth.default.so"

// The actual patch for bluetooth.default.so
#define BTA_SSR_FIND_64BIT  "\xE8\x0B\x40\x79\x0C\x64\x80\x52\x1F\x05\x19\x71"
#define BTA_SSR_PATCH_64BIT "\xE8\x0B\x40\x79\x0c\x00\x80\x52\x1F\x05\x19\x71"
#define BTA_SSR_SIZE_64BIT  12

// Proc Maps stuff
#define PROC_MAPS_MAX_PATH 512
#define PROC_MAPS_SELF "/proc/self/maps"
#define PROC_ADDR_TO_PVOID(addr) ((void*)((uintptr_t)addr))

typedef struct {
    uint64_t start;
    uint64_t end;
    uint64_t size;
    uint32_t r : 1;
    uint32_t w : 1;
    uint32_t x : 1;
    uint32_t p : 1;  // shared (otherwise known as private)
    char path[PROC_MAPS_MAX_PATH];
} proc_maps;

static uint64_t proc_maps_hex(const char *input)
{
    uint64_t address = 0;
    uint8_t byte = 0;
    while (*input) {
        byte = (uint8_t)(*input); 
        input++;
        if (byte >= '0' && byte <= '9')
        {
            byte = byte - '0';
        }
        else if (byte >= 'a' && byte <='f')
        {
            byte = byte - 'a' + 10;
        }
        else if (byte >= 'A' && byte <='F')
        {
            byte = byte - 'A' + 10;
        }
        else
        {
            // bad character, return 0
            return 0;
        }
        address = (address << 4) | (byte & 0xF);
    }
    return address;
}

void proc_maps_copy(char *output, uint32_t size, const char *input, uint32_t begin, uint32_t end)
{
    uint32_t pos = 0;
    uint32_t input_size = 0;
    for (pos = 0; pos < size; pos++)
    {
        output[pos] = '\0';
    }
    if (end < begin)
    {
        // didn't find the end
        return;
    }
    if (end == 0)
    {
        // no input data
        return;
    }
    if (size == 0)
    {
        // no output buffer
        return;
    }
    input_size = end - begin;
    if (input_size > (size - 1))
    {
        // output size minus a nul is not large enough to hold the input size
        return;
    }
    for (pos = 0; pos < input_size; pos++)
    {
        // copy
        output[pos] = input[begin + pos];
    }
}

void proc_maps_parse(proc_maps *info, const char *line, uint32_t line_size)
{
    uint32_t pos_start_begin = 0;
    uint32_t pos_start_end = 0;
    uint32_t pos_end_begin = 0;
    uint32_t pos_end_end = 0;
    uint32_t pos_perm_begin = 0;
    uint32_t pos_perm_end = 0;
    uint32_t pos_path_begin = 0;
    uint32_t pos_path_end = 0;
    uint32_t space_count = 0;
    uint32_t pos = 0;
    for (pos = 0; pos < line_size; pos++)
    {
        char byte = line[pos];
        if (byte == '-')
        {
            if (pos_start_end == 0)
            {
                pos_start_end = pos;
                pos_end_begin = pos + 1;
            }
        }
        if (byte == ' ')
        {
            space_count++;
            if (space_count == 1)
            {
                pos_end_end = pos;
                pos_perm_begin = pos + 1;
            }
            if (space_count == 2)
            {
                pos_perm_end = pos;
            }
        }
        if ((space_count >= 5) && (byte != ' ') && (pos_path_begin == 0))
        {
            pos_path_begin = pos;
        }
        if ((pos_path_begin != 0) && ((byte == '\r') || (byte == '\n')))
        {
            pos_path_end = pos;
        }
    }
    if ((pos_path_begin != 0) && (pos_path_end == 0))
    {
        pos_path_end = line_size;
    }
    proc_maps_copy(info->path, PROC_MAPS_MAX_PATH, line, pos_start_begin, pos_start_end);
    info->start = proc_maps_hex(info->path);
    proc_maps_copy(info->path, PROC_MAPS_MAX_PATH, line, pos_end_begin, pos_end_end);
    info->end = proc_maps_hex(info->path);
    if (info->end > info->start)
    {
        info->size = info->end - info->start;
    }
    else
    {
        info->size = 0;
    }
    proc_maps_copy(info->path,  PROC_MAPS_MAX_PATH, line, pos_perm_begin, pos_perm_end);
    info->r = info->path[0] == 'r' ? 1 : 0;
    info->w = info->path[1] == 'w' ? 1 : 0;
    info->x = info->path[2] == 'x' ? 1 : 0;
    info->p = info->path[3] == 'p' ? 1 : 0;
    proc_maps_copy(info->path, PROC_MAPS_MAX_PATH, line, pos_path_begin, pos_path_end);
}


// ps -A | grep com\.android\.bluetooth  | tr -s ' ' | cut -d ' ' -f 2
// cat /proc/5936/maps | grep bluetooth\.default\.so

#include <linux/syscalls.h>
#include <linux/fcntl.h>
#include <linux/fs.h>
#include <asm/segment.h>
#include <asm/uaccess.h>
#include <linux/buffer_head.h>

struct file *file_open(const char *path, int flags, int rights) 
{
    struct file *filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(KERNEL_DS);//set_fs(get_ds());
    filp = filp_open(path, flags, rights);
    set_fs(oldfs);
    if (IS_ERR(filp)) {
        err = PTR_ERR(filp);
        return NULL;
    }
    return filp;
}

int file_read(struct file *file, unsigned long long offset, unsigned char *data, unsigned int size) 
{
    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(KERNEL_DS);//set_fs(get_ds());

    ret = vfs_read(file, data, size, &offset);

    set_fs(oldfs);
    return ret;
}

void file_close(struct file *file) 
{
    filp_close(file, NULL);
}

static int find_pid(const char *name) {
	struct task_struct *task;

	for_each_process(task) {
        if (NULL != strstr(task->comm, name)) {
		    //printk(KERN_INFO "MATCH: %s State: %ld\tProcess ID: %d\n", task->comm, task->state, task->pid);
            return (int)task->pid;
        } else {
            //printk(KERN_INFO "NOPE!: %s State: %ld\tProcess ID: %d\n", task->comm, task->state, task->pid);
        }
	}
    return -1;
}

void *find_so_maps(int pid, const char *name, int *size) {
    char proc_maps_path[50];
    struct file *f;
    char buf[1024];
    int recd = -1;
    int length = 0;
    int offset = 0;
    proc_maps info;

    // path to proc maps
    snprintf(proc_maps_path, sizeof(proc_maps_path), "/proc/%d/maps", pid);
    proc_maps_path[sizeof(proc_maps_path)  - 1] = '\0';

    printk("opening %s\n", proc_maps_path);
    f = file_open(proc_maps_path, O_RDONLY, 0);
    if (NULL != f) {
        printk("opened\n");
        while (1) {
            recd = file_read(f, offset, buf, (sizeof(buf) - 1));
            if ((recd > 0) && (recd < sizeof(buf))) {
                //printk("read %d bytes\n", recd);
                buf[recd] = '\0';
                length = 0;
                while ((buf[length] != '\0') && (buf[length] != '\n')) {
                    length++;
                }
                if (buf[length] == '\0') {
                    printk("eof, closing\n");
                    file_close(f);
                    return NULL;
                }
                buf[length] = '\0';
                //printk("length: %d\n", length);
                //printk("line: %s\n", buf);
                memset(&info, 0, sizeof(info));
                proc_maps_parse(&info, buf, (uint32_t)length);
                if (NULL != strstr(info.path, name)) {
                    printk("path: %s\n", info.path);
                    printk("found, closing\n");
                    file_close(f);
                    *size = (int)info.size;
                    return PROC_ADDR_TO_PVOID(info.start);
                }
                offset += length + 1;
            } else {
                printk("read failed %d, closing\n", recd);
                file_close(f);
                return NULL;
            }
        }
        file_close(f);
        printk("closed\n");
    }
    printk("done\n");
    return NULL;
}

void *find_so_mm(int pid, const char *name, int *size) {
    struct task_struct *p;
    struct mm_struct *m;
    struct vm_area_struct *v;
    struct file *f;
    const char *path = NULL;
    unsigned long start, end;
    char buf[256];
    *size = 0;
    for_each_process(p) {
        if (p->pid == pid) {
            m = p->mm;
            v = m->mmap;
            while (NULL != v) {
                f = v->vm_file;
                path = NULL;
                if (f) {
                    path = d_path(&(f->f_path), buf, sizeof(buf));
                }
                start = v->vm_start;
                end = v->vm_end;
                if (path) {
                    if (NULL != strstr(path, name)) {
                        // found
                        printk("path: %s\n", path);
                        printk("found\n");
                        *size = (int)(end - start);
                        return (void*)start;
                    }
                }

                // move to next
                v = v->vm_next;
            }
        }
    }
    return NULL;
}

static int mem_read(int pid, void *addr, void *buf, int size) {
    struct task_struct *task;
    int res;
    struct page *page;
    unsigned long page_addr = ((unsigned long)addr) & ~(0xfff);
    unsigned long page_offset = ((unsigned long)addr) & (0xfff);
    unsigned char *mapped_addr;
    if (page_addr < 0x1000) {
        printk("page address should never be less than 0x1000\n");
        return -1;
    }
    if ((page_offset + size) > 0x1000) {
        printk("I have not implemented reading across pages\n");
        return -1;
    }
    for_each_process(task) {
        if ((int)task->pid == pid) {
            //printk("reading process %d\n", pid);
            down_read(&task->mm->mmap_sem);
            res = get_user_pages(
                task,
                task->mm,
                page_addr,
                1,
                1,
                1,
                &page,
                NULL);
            if (res) {
                //printk("mapped page for process %d\n", pid);
                mapped_addr = (unsigned char *)kmap(page);
                if (NULL != mapped_addr) {
                    //printk("reading %d bytes from process %d\n", size, pid);
                    memcpy(buf, mapped_addr + page_offset, size);
                    //printk("read %d bytes from process %d\n", size, pid);
                    page_cache_release(page);
                    //printk("read success\n");
                    res = 0;
                } else {
                    printk("failed to kmap page from process %d\n", pid);
                    res = -1;
                }
            } else {
                printk("failed to map page from process %d\n", pid);
                res = -1;
            }
            up_read(&task->mm->mmap_sem);
            return res;
        }
    }
    printk("process %d not found\n", pid);
    return -1;
}

static int mem_write(int pid, void *addr, const void *buf, int size) {
    struct task_struct *task;
    int res;
    struct page *page;
    unsigned long page_addr = ((unsigned long)addr) & ~(0xfff);
    unsigned long page_offset = ((unsigned long)addr) & (0xfff);
    unsigned char *mapped_addr;
    if (page_addr < 0x1000) {
        printk("page address should never be less than 0x1000\n");
        return -1;
    }
    if ((page_offset + size) > 0x1000) {
        printk("I have not implemented writing across pages\n");
        return -1;
    }
    for_each_process(task) {
        if ((int)task->pid == pid) {
            printk("writing process %d\n", pid);
            down_read(&task->mm->mmap_sem);
            res = get_user_pages(
                task,
                task->mm,
                page_addr,
                1,
                1,
                1,
                &page,
                NULL);
            if (res) {
                printk("mapped page for process %d\n", pid);
                mapped_addr = (unsigned char *)kmap(page);
                if (NULL != mapped_addr) {
                    printk("writing %d bytes to process %d\n", size, pid);
                    memcpy(mapped_addr + page_offset, buf, size);
                    printk("wrote %d bytes to process %d\n", size, pid);
                    page_cache_release(page);
                    printk("write success\n");
                    res = 0;
                } else {
                    printk("failed to kmap page from process %d\n", pid);
                    res = -1;
                }
            } else {
                printk("failed to map page from process %d\n", pid);
                res = -1;
            }
            up_read(&task->mm->mmap_sem);
            return res;
        }
    }
    printk("process %d not found\n", pid);
    return -1;
}

static int patch(int pid, const void *addr, int size, const void *find, const void *patch, int find_patch_size) {
    void *buf;
    int offset;
    int index;
    buf = kmalloc(0x1000, GFP_KERNEL);
    if (NULL == buf) {
        printk("failed to temp memory for reading page\n");
        return -1;
    }
    while (offset < size) {
        if (mem_read(pid, ((char*)addr) + offset, buf, 0x1000)) {
            printk("read failed @ %p\n", ((char*)addr) + offset);
            kfree(buf);
            return -1;
        }
        for (index = 0; index <= (0x1000 - find_patch_size); index++) {
            if (0 == memcmp(((char*)buf) + index, patch, find_patch_size)) {
                printk("existing patch found\n");
                kfree(buf);
                return 0;
            }
            if (0 == memcmp(((char*)buf) + index, find, find_patch_size)) {
                printk("found a patch location\n");
                if (mem_write(pid, ((char*)addr) + offset + index, patch, find_patch_size)) {
                    printk("failed to patch %p\n", ((char*)addr) + offset + index);
                    kfree(buf);
                    return -1;
                } else {
                    printk("patched %p\n", ((char*)addr) + offset + index);
                    kfree(buf);
                    return 0;
                }
            }
        }
        offset += 0x1000;
    }
    printk("a patch or patch location was not found\n");
    kfree(buf);
    return -1;
}

static void joycon_btp(void) {
    int pid = -1;
    char *addr = NULL;
    int size = 0;
    char buf[4];

    pid = find_pid(BTP_TARGET_PROCESS);
    if (pid > 0) {
        // Ability to read files depends on your 'task'
        // Works when you 'insmod' the driver, not so much when you execute on a timer.
        //addr = find_so_maps(pid, BTP_TARGET_SO, &size);
        // So find the shared object (so) using memory manager (mm)
        addr = find_so_mm(pid, BTP_TARGET_SO, &size);
        if (0 == mem_read(pid, addr + 1, buf, 4)) {
            buf[3] = '\0';
            printk("read: '%s'\n", buf);
        }
        if (0 == mem_write(pid, addr + 1, "meh", 3)) {
            buf[3] = '\0';
            printk("wrote: '%s'\n", "meh");
        }
        if (0 == mem_read(pid, addr + 1, buf, 4)) {
            buf[3] = '\0';
            printk("read: '%s'\n", buf);
        }
    }

    printk("com.android.bluetooth pid is %d\n", pid);
    // LEAKS address in bluetooth process, this is not a good/secure practice
    printk("bluetooth.default.so is at %p size is %d bytes\n", addr, size);
    if ((NULL != addr) && (size > 0)) {
        if (patch(pid, addr, size, BTA_SSR_FIND_64BIT, BTA_SSR_PATCH_64BIT, BTA_SSR_SIZE_64BIT)) {
            printk("failed to find and patch\n");
        } else {
            printk("patched\n");
        }
    }
}

static int prev_pid = -1;
static int patch_attemps = 0;
static struct timer_list joycon_btp_timer;

static void joycon_btp_callback(unsigned long data) {
    int pid = -1;
    data = data;  // unused
    pid = find_pid(BTP_TARGET_PROCESS);
    if (prev_pid != pid) {
        // patch multiple times (just incase the module is not loaded/ready)
        patch_attemps = 3;
        prev_pid = pid;
    }
    if (patch_attemps > 0) {
        // only patch when the pid changes
        // this indicates the process has restarted (think airplane mode)
        joycon_btp();
        patch_attemps--;
    }
    mod_timer(&joycon_btp_timer, jiffies + msecs_to_jiffies(BT_PROC_PATCH_INTERVAL_MS));
}

static int joycon_btp_init(void)
{
    printk("Joycon Bluetooth Patcher Init\n");
    joycon_btp();
    setup_timer(&joycon_btp_timer, joycon_btp_callback, 0);
    mod_timer(&joycon_btp_timer, jiffies + msecs_to_jiffies(BT_PROC_PATCH_INTERVAL_MS));
    return 0;
}

static void joycon_btp_exit(void)
{
    printk("Joycon Bluetooth Patcher Exit\n");
    del_timer(&joycon_btp_timer);
}

module_init(joycon_btp_init);
module_exit(joycon_btp_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Peter Rankin, 2018");
MODULE_DESCRIPTION("Joycon Android Bluetooth Lag Patcher");
