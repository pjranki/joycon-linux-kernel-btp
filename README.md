# Patching driver (linux kernel) for Android Bluetooth process
Fixes lag with Nintendo Joy-Con peripherals.
This patch is unlikely to work on other Android devices, you will need to adjust for your ```bluetooth.default.so```
Copyright (c) 2018 Peter Rankin rankstar59@gmail.com

## GPL
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version.

## Use
Every 5 seconds, checks to see if it can patch the bluetooth process - and patches it if it can.
